var conf = {
  CANVAS_WIDTH: 40,
  CANVAS_HEIGHT: 585,
  //milliseceonds
  TICK_TIME: 80,
  MIN_FREQ: 200,
  MAX_FREQ: 1000,
  HAS_STAT: false,
  USE_CHARCODE: false,
  USE_KEYCODE: true,
  COEFF: 1,
  NOTE_DURATION: 550

};

var context, centerX, centerY, canvas;


var iteratorCount = 0;

var objArray = [];

var onPause = false;


var stat;
var synth;


function init() {
  console.log("INIT");

  canvas = document.getElementById("treeCanvas");
  context = canvas.getContext('2d');
  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;


  document.querySelector("#zoneDeSaisie").onkeydown = keydown;
  document.querySelector("#zoneDeSaisie").onkeypress = keypress;
  document.querySelector("#zoneDeSaisie").onkeyup = keyup;

  if (conf.HAS_STAT) {
    stat = new Stats();
    stat.domElement.style.position = 'absolute';
    stat.domElement.style.display = 'block';
    stat.domElement.style.top = '5px';
    stat.domElement.style.right = '5px';
    stat.domElement.style.zIndex = 100;
    document.body.appendChild(stat.domElement);
  }
  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  synth = new Tone.Synth().toMaster();

  var vol = new Tone.Volume(-12);
  synth.chain(vol, Tone.Master);

  tick();
}

function tick() {
  if (onPause) return;
  if (iteratorCount == conf.MAX_ITERATIONS) return;


  window.requestAnimationFrame(tick);

  if (conf.HAS_STAT) {
    stat.update();
  }


  //console.log(iteratorCount++, objArray.length);
}

/**
* r radius
* x abscisse
* y ordonnee
*

*/
function drawInfo() {

  if (r == undefined)
    r = conf.RADIUS;

  if (r <= 0) {
    return;
  }
  context.beginPath();
  context.arc(x, y, r, 0, 2 * Math.PI, false);
  if (fill == undefined) {
    context.fillStyle = conf.FILL_STYLE;
  } else {
    context.fillStyle = fill;
  }
  context.fill();
  if (line == undefined) {
    context.lineWidth = conf.LINE_WIDTH;
  } else {
    context.lineWidth = line;
  }
  if (conf.HAS_STROKE) {
    if (stroke == undefined) {
      context.strokeStyle = conf.STROKE_COLOR;
    } else {
      context.strokeStyle = stroke;
    }
    context.stroke();
  }
}




function randomizer(objDef) {

  var obj = {};


  return obj;
}


function updateVolume() {
  console.log("updateVolume", document.querySelector("#inVolume").value)
  synth.volume.value = (document.querySelector("#inVolume").value);

}

function reset() {

  init();
}

function loadConfig() {}

function updateConfig() {
  reset();
}

function replay() {
  var partoche = document.querySelector("#zoneDeSaisie").value;

  for (var i = 0; i < partoche.length; i++) {

    var charAscii = partoche.charCodeAt(i);

    console.log("charCode", charAscii)

    playSoundIn(charAscii, i);

  }
}


function playKey(evt) {
  playSound((conf.USE_KEYCODE ? evt.keyCode : evt.charCode) * conf.COEFF);
}

function playSound(what) {
  console.log(what)
  synth.triggerAttack(what, Math.round(conf.NOTE_DURATION / 10) / 100);
  // synth.triggerAttackRelease(what,
  //   Math.round(conf.NOTE_DURATION / 10) / 100,
  //   Math.round(when * conf.NOTE_DURATION / 10) / 100);

}



function playSoundIn(what, when) {
  console.log("playSoundIn", what, when)

  console.log(what * conf.COEFF,
    Math.round(conf.NOTE_DURATION / 10) / 100,
    Math.round(when * conf.NOTE_DURATION / 10) / 100);
  synth.triggerAttackRelease(what * conf.COEFF,
    Math.round(conf.NOTE_DURATION / 10) / 100,
    Math.round(when * conf.NOTE_DURATION / 10) / 100);

}

var keypress = function(evt) {
  console.log("keypress", evt);
}

var keyup = function(evt) {
  console.log("keyup", evt);
  synth.triggerRelease();
}

var keydown = function(evt) {
  console.log("keydown", evt);
  playKey(evt);
}