function Mer(controller) {
  this.isInfinite = false;
  this.width = 100;
  this.height = 100;
  this.age = 0;

  this.maxPopulation = 1000;
  this.population = 0;

  this.cellColor = "#efefef";

  this.c = controller;

  this.algues = new Array();
  this.poissons = new Array();
  this.requins = new Array();
  this.cacas = new Array();
  this.crevettes = new Array();

  this.map = Array();

  this.target = this.c.getContext();
  this.drawMode = "square"; //square / circle
  this.init = function() {
    this.width = conf.WIDTH;
    this.height = conf.HEIGHT;
    this.maxPopulation = conf.MAX_POPULATION;

    for (var x = 0; x < this.width; x++) {
      var column = new Array();
      for (var y = 0; y < this.height; y++) {
        var cellule = new Array();
        column.push(cellule);
      }
      this.map.push(column);
    }


    this.populate();
  };

  this.populate = function() {
    console.log("Populate", this.width, this.height)
    var firstCount = 0;
    while (firstCount < this.maxPopulation) {

      //Algues
      if ((conf.HAS_ALGUES) && ((Math.random() * 100) < conf.AlgueCreationProbability)) {
        let posX = Math.floor(Math.random() * this.width);
        let posY = Math.floor(Math.random() * this.height);
        this.map[posX][posY].push(new Algue(this.c, posX, posY));
        firstCount++;
      }
      //poissons
      if ((conf.HAS_POISSONS) && ((Math.random() * 100) < conf.PoissonCreationProbability)) {
        let posX = Math.floor(Math.random() * this.width);
        let posY = Math.floor(Math.random() * this.height);
        this.map[posX][posY].push(new Poisson(this.c, posX, posY));
        firstCount++;
      }
      //poissons
      if ((conf.HAS_CREVETTES) && ((Math.random() * 100) < conf.CrevetteCreationProbability)) {
        let posX = Math.floor(Math.random() * this.width);
        let posY = Math.floor(Math.random() * this.height);
        this.map[posX][posY].push(new Crevette(this.c, posX, posY));
        firstCount++;
      }
      //Requins
      if ((conf.HAS_REQUINS) && ((Math.random() * 100) < conf.RequinCreationProbability)) {
        let posX = Math.floor(Math.random() * this.width);
        let posY = Math.floor(Math.random() * this.height);
        this.map[posX][posY].push(new Requin(this.c, posX, posY));
        firstCount++;

      }
      //cacas
      if ((conf.HAS_CACA) && ((Math.random() * 100) < conf.CacaCreationProbability)) {
        let posX = Math.floor(Math.random() * this.width);
        let posY = Math.floor(Math.random() * this.height);
        this.map[posX][posY].push(new Caca(this.c, posX, posY));
        firstCount++;
      }
    }

    console.log("first count", firstCount)
  };

  this.compute = function() {

    this.age++;

    if (this.age == 10) {
      //  onPause = true;
      //  return;
    }

    for (var x = 0; x < this.width; x++) {
      if (((x % 5) == 0) && (conf.drawModulo5Lines)) {

        this.target.beginPath();
        this.target.lineWidth = "1";
        this.target.strokeStyle = "black";
        this.target.moveTo(x * conf.CELL_SIZE, 0);
        this.target.lineTo(x * conf.CELL_SIZE, conf.HEIGHT * conf.CELL_SIZE);
        this.target.stroke();
      }

      for (var y = 0; y < this.height; y++) {
        if (((y % 5) == 0) && (conf.drawModulo5Lines)) {

          this.target.beginPath();
          this.target.lineWidth = "1";
          this.target.strokeStyle = "black";
          this.target.moveTo(x * conf.CELL_SIZE, y * conf.CELL_SIZE);
          this.target.lineTo((x + 1) * conf.CELL_SIZE, y * conf.CELL_SIZE);
          this.target.stroke();
        }

        //S'il y a du monde ds la cellule
        if (this.map[x][y].length > 0) {

          this.removeObsoletes(x, y);

          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {

            var creature = this.map[x][y][i];

            if ((x != creature.posX) || (y != creature.posY)) {
              console.log("incoherence x,y", x, y, 'creature', creature.posX, creature.posY)
              onPause = true;
              return;
            }

            //  creature.die();
            if (this.population < this.maxPopulation) {
              creature.reproduce(i);
            }

            creature.eat();

            //mouvement
            if (creature.getOld()) { //si creature est pas morte de vieillesse

              if (creature.move()) { //Si nouvelle case

              }
            }
          }
        }
      }
    }

    //TODO : A optimiser parce que c lourd : decompte au moment de la creation / mort
    this.population = this.getRequins().length + this.getPoissons().length + this.getAlgues().length + this.getCrevettes().length;

    this.getCacas();

    if (this.age < 10) {
      console.log("Pop°", this.age, this.population, "r", this.getRequins().length, "p", this.getPoissons().length, "a", this.getAlgues().length, 'cr', this.crevettes.length, "c", this.cacas.length)
    }


    // thi.lengths.populationCount = this.population.length
  };

  this.draw = function() {

    for (var x = 0; x < this.width; x++) {
      for (var y = 0; y < this.height; y++) {

        this.target.beginPath();
        // ctx.lineWidth = "4";
        // ctx.strokeStyle = "green";
        this.target.fillStyle = this.cellColor;
        this.target.strokeStyle = "white";
        this.target.lineWidth = 2;
        this.target.rect(x * conf.CELL_SIZE, y * conf.CELL_SIZE, conf.CELL_SIZE - 1, conf.CELL_SIZE - 1);
        this.target.stroke();
        this.target.fill();

        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (!this.map[x][y][i].isObsolete) {
              this.map[x][y][i].draw();
            }
          }
        }
      }
    }

    this.c.updateDisplay();
  };

  this.getAlgues = function() {
    this.algues = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == "ALGUE") {
              if (!this.map[x][y][i].isObsolete) {
                this.algues.push(this.map[x][y][i]);
              }
            }
          }
        }
      }
    }
    return this.algues;
  };
  this.getCacas = function() {
    this.cacas = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == "CACA") {
              if (!this.map[x][y][i].isObsolete) {
                this.cacas.push(this.map[x][y][i]);
              }
            }
          }
        }
      }
    }
    return this.cacas;
  };

  this.getPoissons = function() {
    this.poissons = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == "POISSON") {
              if (!this.map[x][y][i].isObsolete) {
                this.poissons.push(this.map[x][y][i]);
              }
            }
          }
        }
      }
    }
    return this.poissons;
  };
  this.getCrevettes = function() {
    this.crevettes = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == "CREVETTE") {
              if (!this.map[x][y][i].isObsolete) {
                this.crevettes.push(this.map[x][y][i]);
              }
            }
          }
        }
      }
    }
    return this.crevettes;
  };
  this.getRequins = function() {
    this.requins = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == "REQUIN") {
              if (!this.map[x][y][i].isObsolete) {
                this.requins.push(this.map[x][y][i]);

              }
            }
          }
        }
      }
    }
    return this.requins;
  };

  this.setDimension = function(w, h) {
    this.width = w;
    this.height = h;
  }

  this.removeObsoletes = function(x, y) {
    var deletesCount = 0;
    for (var i = 0; i < this.map[x][y].length; i++) {
      if (this.map[x][y][i].isObsolete) {
        //  console.log("Remove from ", x, y)
        this.map[x][y].splice(i, 1);
        deletesCount++;
      }
    }

    return deletesCount;
  }

}