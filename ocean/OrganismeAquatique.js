class OrganismeAquatique {

  constructor(controller, x, y) {
    this.target = controller.getContext();
    this.posX = x;
    this.posY = y;
    this.mer = controller.getMer();
    //todo METTRE this.mer[posX][posY].push(this)... du genre
  }


  type = "OrganismeAquatique";
  proieType = "";
  creationProbability = 5;
  natality = 5;
  color = "#000000";
  mobility = 1;
  lifeCycles = 3;
  age = 0;
  doesPoop = true;
  //doit etre supprimé de la cellule au prochain tour (est mort ou a bougé )
  isObsolete = false;

  static getColor() {
    return this.color;
  }

  static getCount(mer) {
    let populationCount = 0;
    console.log(this.type, type)
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == this.type) {
              if (!this.map[x][y][i].isObsolete) {
                populationCount++;
              }
            }
          }
        }
      }
    }
    return populationCount;
  };

  static getPopulation(mer) {
    let population = new Array();
    for (var x = 0; x < this.width - 1; x++) {
      for (var y = 0; y < this.height - 1; y++) {
        if (this.map[x][y].length > 0) {
          //console.log("life")
          for (var i = 0; i < this.map[x][y].length; i++) {
            if (this.map[x][y][i].type == this.type) {
              if (!this.map[x][y][i].isObsolete) {
                population.push(this.map[x][y][i]);
              }
            }
          }
        }
      }
    }
    return population();
  };
  eat() {
    if (!this.isObsolete) {
      //  console.log("SHOULDNT BE THERE !", this.posX, this.posY)

      for (var i = 0; i < this.mer.map[this.posX][this.posY].length; i++) {
        if ((this.mer.map[this.posX][this.posY][i].type == this.proieType) && (!this.mer.map[this.posX][this.posY][i].isObsolete)) {
          //console.log("miam")
          this.mer.map[this.posX][this.posY][i].die();
          if (this.doesPoop) {
            this.faitCaca();
          }
          this.age = 0;
          return true;
          //  this.age = 0;
        }
      }
    }
    return false;
  }

  reproduce() {
    if (!this.isObsolete) {
      if (this.age < 5) {
        if ((Math.round(Math.random() * 100)) < this.natality) {



          //  console.log("birth")
          var newOrganisme = new this.constructor(controller, this.posX, this.posY);
          newOrganisme.age = 0;
          this.mer.map[this.posX][this.posY].push(newOrganisme);
          this.mer.population++;
        }
      }
    }
  }

  getOld() {
    if (!this.isObsolete) {
      if (this.age >= this.lifeCycles) {
        //  console.log("dies old", this);
        this.die();
        return false;
      } else {
        this.age++;
        return true;
      }
    }
  };

  die() {
    if (!this.isObsolete) {
      this.isObsolete = true;
      this.mer.population--;
      return true;
    }
    return false;
  }

  move() {
    if (!this.isObsolete) {
      let newX = this.posX + Math.floor(Math.random() * (1 + this.mobility * 2)) - (this.mobility);
      let newY = this.posY + Math.floor(Math.random() * (1 + this.mobility * 2)) - (this.mobility);

      if (this.mer.isInfinite) {

        if (newX < 0) {
          newX += this.mer.width;
        }
        if (newX >= this.mer.width) {
          newX -= this.mer.width;
        }
        if (newY < 0) {
          newY += this.mer.height;
        }
        if (newY >= this.mer.height) {
          newY -= this.mer.height;
        }
      } else {
        if (newX < 0) {
          newX = 0;
        }
        if (newX >= this.mer.width) {
          newX = this.mer.width - 1
        }
        if (newY < 0) {
          newY = 0;
        }
        if (newY >= this.mer.height) {
          newY = this.mer.height - 1
        }
      }

      //Boolean : nouvelle cellule
      let isNewCell = ((this.posX != newX) || (this.posY != newY));
      if (isNewCell) {
        //  console.log("Move from ", this.posX, this.posY, "to", newX, newY)
      }

      if (isNewCell) {
        let oldX = this.posX;
        let oldY = this.posY;


        //  console.log("Moving from ", oldX, oldY, "to", newX, newY);

        try {
          let newOrgAqua = this.clone();
          newOrgAqua.posX = newX;
          newOrgAqua.posY = newY;
          this.isObsolete = true;
          //    console.log(this, newOrgAqua)
          this.mer.map[newX][newY].push(newOrgAqua);

          //console.log("age", this.age, this.mer.map[oldX][oldY].length, this.mer.map[this.posX][this.posY].length, this.mer.map[newX][newY].length);

        } catch (e) {
          console.log("Error Mer", // console.log("Error Mer",

            newX, newY, this.type, e);
        }
      }

      return isNewCell;
    }
    return false;
  };

  faitCaca() {

    if (!this.isObsolete) {
      this.mer.map[this.posX][this.posY]
        .push(new Caca(controller, this.posX, this.posY));
    }

  }

  clone() {
    //OLD  return Object.assign({}, this);

    var theClone = new this.constructor(controller, this.posX, this.posY);
    theClone.age = this.age;
    return theClone;
  }

  draw() {
    switch (this.mer.drawMode) {
      case "circle":
        this.drawAsCircle();
        break;
      case "square":
      default:
        this.drawAsSquare();
        break;
    }
  }

  drawAsSquare() {

    this.target.beginPath();
    // ctx.lineWidth = "4";
    // ctx.strokeStyle = "green";
    this.target.fillStyle = this.color + conf.CREATURE_ALPHA_FILL;
    this.target.strokeStyle = "lightgray";
    this.target.lineWidth = 0.5;
    this.target.rect(this.posX * conf.CELL_SIZE, this.posY * conf.CELL_SIZE, conf.CELL_SIZE - 1, conf.CELL_SIZE - 1);
    this.target.fill();

  }
  drawAsCircle() {

    let r = conf.RADIUS;

    context.beginPath();
    context.arc(this.posX * conf.CELL_SIZE, this.posY * conf.CELL_SIZE, conf.CELL_SIZE / 2, 0, 2 * Math.PI, false);

    context.fillStyle = this.color + conf.CREATURE_ALPHA_FILL;
    context.fill();
  }


}