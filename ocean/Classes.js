class Caca extends OrganismeAquatique {
  constructor(controller, x, y) {
    super(controller, x, y);
    this.type = "CACA";
    this.proieType = "";
    this.creationProbability = 15;
    this.natality = 0;
    this.mobility = 0;
    this.lifeCycles = 1000;
    this.color = "#bbbb00";
    this.doesPoop = false;
  }
}

class Algue extends OrganismeAquatique {
  constructor(controller, x, y) {
    super(controller, x, y);
    this.type = "ALGUE";
    this.proieType = "CACA";
    this.creationProbability = 25;
    this.natality = 25;
    this.mobility = 1;
    this.lifeCycles = 100;
    this.color = "#00bb00";
    this.doesPoop = false;
  }
}

class Crevette extends OrganismeAquatique {
  constructor(controller, x, y) {
    super(controller, x, y);
    this.type = "CREVETTE";
    this.proieType = "ALGUE";
    this.creationProbability = 10;
    this.natality = 20;
    this.mobility = 1;
    this.lifeCycles = 40;
    this.color = "#770077";
    // this.color = "#db5788";
  }
}

class Poisson extends OrganismeAquatique {
  constructor(controller, x, y) {
    super(controller, x, y);
    this.type = "POISSON";
    this.proieType = "ALGUE";
    // this.proieType = "CREVETTE";
    this.creationProbability = 10;
    this.natality = 20;
    this.mobility = 1;
    this.lifeCycles = 40;
    this.color = "#0000bb";
  }
}

class Requin extends OrganismeAquatique {
  constructor(controller, x, y) {
    super(controller, x, y);
    this.type = "REQUIN";
    this.proieType = "POISSON";
    this.creationProbability = 15;
    this.natality = 25;
    this.mobility = 1;
    this.lifeCycles = 70;
    this.color = "#bb0000";
  }
}