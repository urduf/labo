var conf = {
  hasStat: true,
  CANVAS_WIDTH: 500,
  CANVAS_HEIGHT: 400,

  //milliseceonds
  TICK_TIME: 80,
  WIDTH: 100,
  HEIGHT: 50,
  CELL_SIZE: 8,
  drawModulo5Lines: false,
  //TODO : mettre en methode/propriete statique ds classes
  AlgueCreationProbability: 45,
  PoissonCreationProbability: 35,
  RequinCreationProbability: 15,
  CacaCreationProbability: 35,
  CrevetteCreationProbability: 25,
  CREATURE_ALPHA_FILL: "50",
  MAX_POPULATION: 300,
  HAS_ALGUES: true,
  HAS_POISSONS: true,
  HAS_REQUINS: false,
  HAS_CACA: true,
  HAS_CREVETTES: false,
  DRAW_MODE: "circle",
  INFINITE_SEA: true
};

var context, centerX, centerY, canvas;

var iteratorCount = 0;

var objArray = [];

var onPause = false;

var isStarted = false;

var stat;
var ocean;



function init() {
  console.log("INIT");

  canvas = document.getElementById("treeCanvas");
  canvasStat = document.getElementById("statMonitorCanvas");
  context = canvas.getContext('2d');
  contextStat = canvasStat.getContext('2d');

  conf.CANVAS_WIDTH = conf.CELL_SIZE * (conf.WIDTH - 1);
  conf.CANVAS_HEIGHT = conf.CELL_SIZE * (conf.HEIGHT - 1);

  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;
  canvasStat.width = conf.CANVAS_WIDTH;

  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;

  canvas.onclick = function() {

  };

  controller = new Controller();
  controller.setContext(context);
  controller.setStatContext(contextStat);


  ocean = new Mer(controller);
  controller.setMer(ocean);

  ocean.setDimension(conf.WIDTH, conf.HEIGHT);
  ocean.drawMode = conf.DRAW_MODE;
  ocean.isInfinite = conf.INFINITE_SEA;
  ocean.init();

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }
  // context.translate(centerX, centerY);

  if (!isStarted) {

    start();
  }
}

function start() {
  isStarted = true;
  tick();
}

function tick() {
  if (onPause) return;
  ocean.compute();

  ocean.draw();



  window.requestAnimationFrame(tick);

  if (conf.hasStat) {
    stat.update();
  }

  //console.log(iteratorCount++, objArray.length);
}

/**
* r radius
* x abscisse
* y ordonnee
*

*/
function drawCircle(x, y, r, fill, line, stroke) {

  if (r == undefined)
    r = conf.RADIUS;

  if (r <= 0) {
    return;
  }
  context.beginPath();
  context.arc(x, y, r, 0, 2 * Math.PI, false);
  if (fill == undefined) {
    context.fillStyle = conf.FILL_STYLE;
  } else {
    context.fillStyle = fill;
  }
  context.fill();
  if (line == undefined) {
    context.lineWidth = conf.LINE_WIDTH;
  } else {
    context.lineWidth = line;
  }
  if (conf.HAS_STROKE) {
    if (stroke == undefined) {
      context.strokeStyle = conf.STROKE_COLOR;
    } else {
      context.strokeStyle = stroke;
    }
    context.stroke();
  }
}

function drawCircleObj(obj) {
  drawCircle(obj.x, obj.y, obj.radius, obj.color);
}


function randomizer(objDef) {
  //console.log(objDef.radius);
  var obj = {};

  var delta = ((!conf.FIXED_DIVERGENCE) &&
      (conf.DIVERGENCE_RATIO > objDef.radius)) ?
    objDef.radius :
    conf.DIVERGENCE_RATIO;

  //console.log(objDef.radius == delta);

  var x = Math.floor(Math.random() * 3) - 1;

  obj.x = objDef.x + x * delta;
  if ((!conf.OVERFLOW) && (
      (obj.x + objDef.radius + conf.BORDER_SIZE > canvas.clientWidth) ||
      (obj.x - objDef.radius - conf.BORDER_SIZE < 0)
    )) {
    obj.x = objDef.x;
  }

  obj.y = objDef.y;

  obj.radius = objDef.radius - conf.SIZE_STEP;

  var color = objDef.color;
  var rCol = parseInt(color.substring(1, 3), 16);
  var vCol = parseInt(color.substring(3, 5), 16);
  var bCol = parseInt(color.substring(5, 7), 16);

  rCol--;
  bCol--;
  vCol--;

  obj.color = "#" + toHex256(rCol) +
    toHex256(vCol) +
    toHex256(bCol) +
    toHex256(conf.FILL_ALPHA);

  obj.rotation = objDef.rotation + conf.ROTATION_GAP;

  //console.log(obj, color, rCol, vCol, bCol);
  //console.log(color);

  return obj;
}


function redraw() {
  if (onPause) {
    onPause = false;
  }
  //window.cancelAnimationFrame()
  init();
}

function togglePause() {
  onPause = !onPause;
  document.querySelector("#btnPlayPause").classList.add(onPause ? "fa-play" : "fa-pause");
  document.querySelector("#btnPlayPause").classList.remove(onPause ? "fa-pause" : "fa-play");
  if (!onPause) {
    tick();
  }
}

function loadConfig() {}

function updateConfig() {
  conf.MAX_ITERATIONS = parseInt(document.getElementById("inMaxIterations").value);
  conf.CANVAS_WIDTH = parseInt(document.getElementById("inCanvasWidth").value);
  conf.CANVAS_HEIGHT = parseInt(document.getElementById("inCanvasHeight").value);
  conf.RADIUS = parseInt(document.getElementById("inRadius").value);
  conf.DIVERGENCE_RATIO = parseFloat(document.getElementById("inDivergenceRatio").value);
  conf.SIZE_STEP = parseFloat(document.getElementById("inSizeStep").value);
  conf.START_X = parseFloat(document.getElementById("inStartX").value);
  conf.START_Y = parseFloat(document.getElementById("inStartY").value);
  conf.MAX_BRANCHES = parseInt(document.getElementById("inMaxBranches").value);
  conf.FILL_STYLE = document.getElementById("inFillStyle").value;
  conf.RANDOM_COLOR = document.getElementById("inRandomColor").checked;
  conf.FILL_ALPHA = parseInt(document.getElementById("inFillAlpha").value);
  conf.HAS_STROKE = document.getElementById("inHasStroke").checked;
  conf.LINE_WIDTH = parseFloat(document.getElementById("inLineWidth").value);
  conf.OVERFLOW = document.getElementById("inOverflow").checked;
  conf.BORDER_SIZE = parseFloat(document.getElementById("inBorder").value);
  conf.FIXED_DIVERGENCE = document.getElementById("inFixedDivergence").checked;
  conf.ROTATION_GAP = parseFloat(document.getElementById("inRotationGap").value);
  redraw();
}