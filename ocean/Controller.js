var Controller = function() {
  this.context;
  this.contextStat;
  this.mer;
  this.setMer = function(o) {
    this.mer = o;
  };
  this.getMer = function() {
    return this.mer;
  };
  this.setContext = function(c) {
    this.context = c;
  };
  this.setStatContext = function(c) {
    this.contextStat = c;
  };
  this.getContext = function() {
    return this.context;
  };
  this.updateDisplay = function() {

    document.querySelector("#spAgeMer").innerHTML = this.mer.age;
    document.querySelector("#spPopGlobale").innerHTML =
      this.mer.population + "/" + this.mer.maxPopulation;

    let cacasPercentage =
      Math.round((this.mer.cacas.length / this.mer.population) * 100);
    let crevettesPercentage =
      Math.round((this.mer.crevettes.length / this.mer.population) * 100);
    let alguesPercentage =
      Math.round((this.mer.algues.length / this.mer.population) * 100);
    let poissonsPercentage =
      Math.round((this.mer.poissons.length / this.mer.population) * 100);
    let requinsPercentage =
      Math.round((this.mer.requins.length / this.mer.population) * 100);

    document.querySelector("#spPopCacas").innerHTML =
      this.mer.cacas.length;
    document.querySelector("#spPopAlgues").innerHTML =
      this.mer.algues.length + " - " + alguesPercentage + "%";
    document.querySelector("#spPopPoissons").innerHTML =
      this.mer.poissons.length + " - " + poissonsPercentage + "%";
    document.querySelector("#spPopRequins").innerHTML =
      this.mer.requins.length + " - " + requinsPercentage + "%";
    document.querySelector("#spPopCrevettes").innerHTML =
      this.mer.crevettes.length + " - " + crevettesPercentage + "%";


    let statXWritePosition =
      (this.mer.age >= this.contextStat.canvas.width) ?
      this.contextStat.canvas.width - 1 : this.mer.age;

    if (this.mer.age >= this.contextStat.canvas.width) {
      //  this.contextStat.translate(1, 0)
      let imageData =
        this.contextStat.getImageData(1, 0, this.contextStat.canvas.width - 1, this.contextStat.canvas.height);
      this.contextStat.clearRect(0, 0, this.contextStat.canvas.width, this.contextStat.canvas.height);
      this.contextStat.putImageData(imageData, 0, 0, 0, 0, this.contextStat.canvas.width, this.contextStat.canvas.height);

    }

    if (this.mer.cacas.length > 0) {
      this.contextStat.beginPath(); // Start a new path
      this.contextStat.strokeStyle = this.mer.cacas[0].color + "26";
      this.contextStat.moveTo(statXWritePosition, 100 - cacasPercentage);
      this.contextStat.lineTo(statXWritePosition + 1, 100);
      this.contextStat.stroke();
    }
    if (this.mer.algues.length > 0) {
      this.contextStat.beginPath(); // Start a new path
      this.contextStat.strokeStyle = this.mer.algues[0].color;
      this.contextStat.moveTo(statXWritePosition, 100 - alguesPercentage);
      this.contextStat.lineTo(statXWritePosition + 1, 100 - alguesPercentage);
      this.contextStat.stroke();
    }
    if (this.mer.crevettes.length > 0) {
      this.contextStat.beginPath(); // Start a new path
      this.contextStat.strokeStyle = this.mer.crevettes[0].color;
      this.contextStat.moveTo(statXWritePosition, 100 - crevettesPercentage);
      this.contextStat.lineTo(statXWritePosition + 1, 100 - crevettesPercentage);
      this.contextStat.stroke();
    }
    if (this.mer.poissons.length > 0) {
      this.contextStat.beginPath(); // Start a new path
      this.contextStat.strokeStyle = this.mer.poissons[0].color;
      this.contextStat.moveTo(statXWritePosition, 100 - poissonsPercentage);
      this.contextStat.lineTo(statXWritePosition + 1, 100 - poissonsPercentage);
      this.contextStat.stroke();
    }
    if (this.mer.requins.length > 0) {
      this.contextStat.beginPath(); // Start a new path
      this.contextStat.strokeStyle = this.mer.requins[0].color;
      this.contextStat.moveTo(statXWritePosition, 100 - requinsPercentage);
      this.contextStat.lineTo(statXWritePosition + 1, 100 - requinsPercentage);
      this.contextStat.stroke();
    }




    this.context.font = '50px Arial';
    if (alguesPercentage == 100) {
      this.context.fillStyle = this.mer.algues[0].color;
      this.context.fillText('les algues ont gagné !', 50, 90);
      onPause = true;
    }
    if (crevettesPercentage == 100) {
      this.context.fillStyle = this.mer.crevettes[0].color;
      this.context.fillText('les crevettes ont gagné !', 50, 90);
      onPause = true;
    }
    if (poissonsPercentage == 100) {
      this.context.fillStyle = this.mer.poissons[0].color;
      this.context.fillText('les poissons ont gagné !', 50, 90);
      onPause = true;
    }
    if (requinsPercentage == 100) {
      this.context.fillStyle = this.mer.requins[0].color;
      this.context.fillText('les requins ont gagné !', 50, 90);
      onPause = true;
    }
  }
}