var conf = {
  hasStat: false,
  CANVAS_WIDTH: 300,
  CANVAS_HEIGHT: 300,
  MAX_ITERATIONS: 255,
  //milliseceonds
  TICK_TIME: 80,
  RADIUS: 110,
  SIZE_STEP: .7,
  MAX_BRANCHES: 780,
  FILL_STYLE: '#ffbbbb',
  //0..256
  LINE_WIDTH: 0.7,
  ROTATION_GAP: 60,
  DRAW_COLOR: "#000000",
  DRAW_ALPHA: 164,
  DRAW_SIZE: 1,
  SHADOW_COLOR: "#000000",
  SHADOW_SIZE: 30
};

var context, centerX, centerY, canvas, cursorCanvas, drawCursor;

var arrPoints = new Array();
var arrLines = new Array();

var penIsDown = false;
var stat;


function init() {
  console.log("INIT");


  restorePaletteFromCookie();

  drawCursor = new DrawCursor();

  arrColors.push(conf.DRAW_COLOR);

  canvas = document.getElementById("drawCanvas");
  context = canvas.getContext('2d');

  context.shadowColor = conf.SHADOW_COLOR;
  context.shadowBlur = conf.SHADOW_SIZE;



  cursorCanvas = document.getElementById("cursorCanvas");
  contextCursorCanvas = cursorCanvas.getContext('2d');

  drawCursor.setTarget(contextCursorCanvas);
  drawCursor.setSize(conf.DRAW_SIZE);

  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  cursorCanvas.width = conf.CANVAS_WIDTH;
  cursorCanvas.height = conf.CANVAS_HEIGHT;

  drawCursor.init();

  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;

  context.translate(centerX, centerY);
  contextCursorCanvas.translate(centerX, centerY);

  context.lineCap = "round";
  context.lineJoin = 'round';

  recordState();

  cursorCanvas.onmousedown = function(evt) {
    mousedown(evt);
  };
  cursorCanvas.onmouseup = function(evt) {
    mouseup(evt);
  };
  cursorCanvas.onmousemove = function(evt) {
    mousemove(evt);
  };
  cursorCanvas.onmouseout = function(evt) {
    mouseout(evt);
  };

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }

  document.querySelectorAll("#divFormConfig input").forEach(function(elem) {
    elem.onchange = updateConfig;
  });
  document.querySelector("#inRotationGapSelect").onchange = function(elem) {
    document.querySelector("#inRotationGap").value = document.querySelector("#inRotationGapSelect").selectedOptions[0].value;
    updateConfig();
  };
  document.querySelector("#inDrawSizeRange").onchange = function(elem) {
    document.querySelector("#inDrawSize").value = document.querySelector("#inDrawSizeRange").value;
    updateConfig();
  };

}


function mousedown(evt) {
  // console.log(evt);
  arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
  penIsDown = true;
}

function mousemove(evt) {
  //console.log(evt);
  if (penIsDown) {
    arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
    drawLine();
  }
  drawCursor.draw(evt.layerX - centerX, evt.layerY - centerY);
}


function mouseup(evt) {
  //console.log(evt);
  penIsDown = false;
  //console.log("Sending", arrPoints.length, "points", arrPoints);
  arrLines.push(arrPoints);
  recordState();
  arrPoints = new Array();
}

function mouseout(evt) {
  if (penIsDown) {
    mouseup(evt);
  }
}


function drawLine() {
  // let region = new Path2D();
  // // region.beginPath();
  // region.moveTo(0, 0);
  // region.lineTo(centerX, 0);
  /*
  X=distance*cos(angle) +x0
  Y=distance*sin(angle) +y0

  https://code-examples.net/fr/q/10a5e8f
  */

  context.shadowColor = conf.SHADOW_COLOR;
  context.shadowBlur = conf.SHADOW_SIZE;

  // region.lineTo(
  //   Math.round(centerX * Math.cos(conf.ROTATION_GAP * Math.PI / 180)),
  //   Math.round(centerX * Math.sin(conf.ROTATION_GAP * Math.PI / 180)));
  // // region.lineTo(0, 0); //closepath e fait automatiquement
  // region.closePath();
  // context.clip(region);
  // context.stroke(region);
  var nbIterations = Math.ceil(360 / conf.ROTATION_GAP)
  // console.log(nbIterations);
  context.save();

  // context.beginPath();
  // context.moveTo(0, 0);
  // context.lineTo(centerX, 0);
  // context.lineTo(
  //   Math.round(centerX * Math.cos(conf.ROTATION_GAP * Math.PI / 180)),
  //   Math.round(centerX * Math.sin(conf.ROTATION_GAP * Math.PI / 180)));
  //
  // context.closePath();
  // context.clip();

  for (var i = 0; i < nbIterations; i++) {
    context.save();
    if (i % 2 == 1) {
      context.scale(1, -1);
    }
    if (arrPoints.length > 1) {
      context.beginPath();
      context.strokeStyle = conf.DRAW_COLOR + conf.DRAW_ALPHA;
      context.lineWidth = conf.DRAW_SIZE;
      context.moveTo(arrPoints[arrPoints.length - 2][0], arrPoints[arrPoints.length - 2][1]);

      context.lineTo(arrPoints[arrPoints.length - 1][0], arrPoints[arrPoints.length - 1][1]);
      context.stroke();
    }

    context.restore();
    context.rotate(conf.ROTATION_GAP * Math.PI / 180);
  }
  context.restore();
}


function updateConfig() {
  console.log("updateConfig")
  conf.DRAW_SIZE = document.getElementById("inDrawSize").value;
  conf.DRAW_COLOR = document.getElementById("inDrawColor").value;
  conf.DRAW_ALPHA = toHex256(parseInt(document.getElementById("inDrawAlpha").value));
  conf.SHADOW_SIZE = parseInt(document.getElementById("inShadowSize").value);
  conf.SHADOW_COLOR = document.getElementById("inShadowColor").value;

  conf.ROTATION_GAP = parseInt(document.getElementById("inRotationGap").value);
  updateColorTable();
  updateColorShadowTable();
  drawCursor.setSize(document.getElementById("inDrawSize").value);
  resetDrawPosition();
}