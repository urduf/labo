var conf = {
  hasStat: false,
  CANVAS_WIDTH: 830,
  CANVAS_HEIGHT: 585,
  MAX_ITERATIONS: 255,
  //milliseceonds
  TICK_TIME: 80,
  RADIUS: 110,
  DIVERGENCE_RATIO: 12.3,
  SIZE_STEP: .7,
  MAX_BRANCHES: 780,
  FILL_STYLE: '#ffbbbb',
  //0..256
  FILL_ALPHA: 164,
  RANDOM_COLOR: true,
  STROKE_COLOR: '#333333',
  HAS_STROKE: false,
  LINE_WIDTH: 0.7,
  DIVISION_CHANCES: 04,
  OVERFLOW: false,
  BORDER_SIZE: 10,
  FIXED_DIVERGENCE: true
};

var context, centerX, centerY, canvas;


var iteratorCount = 0;

var objArray = [];

var onPause = false;

var isStarted = false;

var stat;

function init() {
  console.log("INIT");

  canvas = document.getElementById("treeCanvas");
  context = canvas.getContext('2d');
  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;


  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  var obj = {
    x: centerX,
    y: centerY,
    radius: conf.RADIUS,
    color: conf.FILL_STYLE
  };

  objArray.push(obj);

  //context.translate(centerX, centerY);

if(!isStarted){
    canvas.onclick = function() {
    onPause = !onPause;
    if (!onPause) {
      tick();
    }
  };

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }

    start();
  }
}

function start() {
  isStarted = true;
  tick();
}

function tick() {
  if (onPause) return;
  if (iteratorCount == conf.MAX_ITERATIONS) return;

  for (var i = 0; i < objArray.length; i++) {

    var objTmp = objArray[i];
    var newObj = randomizer(objTmp);

    if (objArray[i].radius <= 0) {
      objArray.splice(i, 1);
      tick();
      return;
    } else {

      drawCircleObj(newObj);
      if (objArray.length < conf.MAX_BRANCHES) {

        if (Math.random() * 100 < conf.DIVISION_CHANCES) {
          // console.log("division !", objArray.length);
          objArray.push(newObj);
          if (conf.RANDOM_COLOR) {

            objArray[objArray.length - 1].color = randomColor();
          }
        }
      }
    }
    objArray[i] = newObj;
  }


  window.requestAnimationFrame(tick);
  if (conf.hasStat) {
    stat.update();
  }

  //console.log(iteratorCount++, objArray.length);
}

/**
* r radius
* x abscisse
* y ordonnee
*

*/
function drawCircle(x, y, r, fill, line, stroke) {

  if (r == undefined)
    r = conf.RADIUS;

  if (r <= 0) {
    return;
  }
  context.beginPath();
  context.arc(x, y, r, 0, 2 * Math.PI, false);
  if (fill == undefined) {
    context.fillStyle = conf.FILL_STYLE;
  } else {
    context.fillStyle = fill;
  }
  context.fill();
  if (line == undefined) {
    context.lineWidth = conf.LINE_WIDTH;
  } else {
    context.lineWidth = line;
  }
  if (conf.HAS_STROKE) {
    if (stroke == undefined) {
      context.strokeStyle = conf.STROKE_COLOR;
    } else {
      context.strokeStyle = stroke;
    }
    context.stroke();
  }
}

function drawCircleObj(obj) {
  drawCircle(obj.x, obj.y, obj.radius, obj.color);
}


function randomizer(objDef) {
  //console.log(objDef.radius);
  var obj = {};

  var delta = ((!conf.FIXED_DIVERGENCE) &&
      (conf.DIVERGENCE_RATIO > objDef.radius)) ?
    objDef.radius :
    conf.DIVERGENCE_RATIO;

  //console.log(objDef.radius == delta);

  var x = Math.floor(Math.random() * 3) - 1;

  obj.x = objDef.x + x * delta;
  if ((!conf.OVERFLOW) && (
      (obj.x + objDef.radius + conf.BORDER_SIZE > canvas.clientWidth) ||
      (obj.x - objDef.radius - conf.BORDER_SIZE < 0)
    )) {
    obj.x = objDef.x;
  }
  var y = Math.floor(Math.random() * 3) - 1;
  obj.y = objDef.y + y * delta;
  if ((!conf.OVERFLOW) && (
      (obj.y + objDef.radius + conf.BORDER_SIZE > canvas.clientHeight) ||
      (obj.y - objDef.radius - conf.BORDER_SIZE < 0)
    )) {
    obj.y = objDef.y;
  }

  obj.radius = objDef.radius - conf.SIZE_STEP;

  var color = objDef.color;
  var rCol = parseInt(color.substring(1, 3), 16);
  var vCol = parseInt(color.substring(3, 5), 16);
  var bCol = parseInt(color.substring(5, 7), 16);

  rCol--;
  bCol--;
  vCol--;

  obj.color = "#" + toHex256(rCol) +
    toHex256(vCol) +
    toHex256(bCol) +
    toHex256(conf.FILL_ALPHA);

  //console.log(obj, color, rCol, vCol, bCol);
  //console.log(color);

  return obj;
}

function toHex256(intVal) {
  if (intVal <= 0) {
    return "00";
  }
  var hex = intVal.toString(16).toUpperCase();
  if (hex.length < 2) {
    hex = "0" + hex;
  }
  return hex;
}

function randomColor() {
  var hexColor = "#";
  var rCol = Math.floor(Math.random() * 256);
  var vCol = Math.floor(Math.random() * 256);
  var bCol = Math.floor(Math.random() * 256);
  hexColor += toHex256(rCol) + toHex256(vCol) + toHex256(bCol);
  return hexColor;
}

function redraw() {
  objArray = [];
  context.clearRect(0, 0, canvas.width, canvas.height);
  init();
}

function saveImage() {
  var image = canvas.toDataURL("image/png")
    .replace("image/png", "image/octet-stream");
  // here is the most important part because if you dont replace you will get a DOM 18 exception.

  window.location.href = image;
}

function loadConfig() {}

function updateConfig() {
  conf.MAX_ITERATIONS = parseInt(document.getElementById("inMaxIterations").value);
  conf.CANVAS_WIDTH = parseInt(document.getElementById("inCanvasWidth").value);
  conf.CANVAS_HEIGHT = parseInt(document.getElementById("inCanvasHeight").value);
  conf.RADIUS = parseInt(document.getElementById("inRadius").value);
  conf.DIVERGENCE_RATIO = parseFloat(document.getElementById("inDivergenceRatio").value);
  conf.SIZE_STEP = parseFloat(document.getElementById("inSizeStep").value);
  conf.MAX_BRANCHES = parseInt(document.getElementById("inMaxBranches").value);
  conf.FILL_STYLE = document.getElementById("inFillStyle").value;
  conf.RANDOM_COLOR = document.getElementById("inRandomColor").checked;
  conf.FILL_ALPHA = parseInt(document.getElementById("inFillAlpha").value);
  conf.HAS_STROKE = document.getElementById("inHasStroke").checked;
  conf.LINE_WIDTH = parseFloat(document.getElementById("inLineWidth").value);
  conf.OVERFLOW = document.getElementById("inOverflow").checked;
  conf.BORDER_SIZE = parseFloat(document.getElementById("inBorder").value);
  conf.FIXED_DIVERGENCE = document.getElementById("inFixedDivergence").checked;
  redraw();
}
