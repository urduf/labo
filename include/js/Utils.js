/**
 * Randomiseur : pioche un entier au hasard entre 'mini' et 'maxi' inclus
 * Note : Si maxi n'est pas définit, il pioche entre 0 et mini inclus
 * Exemples :
 * randomInt(3,5) => 3,4,ou 5
 * rendomInt(4)=>0,1,2,3 ou 4
 * @param {type} mini
 * @param {type} maxi
 * @returns {Number}
 */
function randomInt(mini, maxi) {
  if (maxi === undefined) {
    maxi = mini;
    mini = 0;
  }
  var nb = mini + (maxi + 1 - mini) * Math.random();
  return Math.floor(nb);
}


/**
 * Melange un tableau javascript : pas d'affectation c'est le tableau qui est mixé direct(php style)
 * @param {Array} array Tableau à mélanger
 * @param {Number::Integer} n Optionnel
 * @return {Undefined}
 * */
function arrayShuffle(array, n) {
  if (!n)
    n = array.length;
  if (n > 1) {
    var i = randomInt(0, n - 1);
    var tmp = array[i];
    array[i] = array[n - 1];
    array[n - 1] = tmp;
    arrayShuffle(n - 1);
  }
}

/**
 * Function permettant le classement d'un tableau composé de nombres
 * Utilisation : Array.sort(sortNumber);
 * @see http://stackoverflow.com/questions/1063007/how-to-sort-an-array-of-integers-correctly#1063027
 * @param {Number} a
 * @param {Number} b
 * @returns {Number}
 */
function sortNumber(a, b) {
  return a - b;
}

/**
 *Melange un tableau javascript (affectation car "return")
 *@param {Array} a
 *@return {Array} le tableau mélangé
 * */
function shuffle(a) {
  var j = 0;
  var valI = '';
  var valJ = valI;
  var l = a.length - 1;
  while (l > -1) {
    j = Math.floor(Math.random() * l);
    valI = a[l];
    valJ = a[j];
    a[l] = valJ;
    a[j] = valI;
    l = l - 1;
  }
  return a;
}

//HTML//////////////////////////////////////////////////////////////////////////

/**
 * Récupère toutes les variables passée en GET sur une page web pour utilisation en js
 * Retourne un objet pouvant être utilisé comme un tableau associatif
 * Sur le modèle de php
 * @returns {Object} Association clef : exemple $_GET["toto"]
 */
function get_html_get_vars() {
  // get query arguments
  var $_GET = {},
    args = location.search.substr(1).split(/&/);
  for (var i = 0; i < args.length; ++i) {
    var tmp = args[i].split(/=/);
    if (tmp[0] !== "") {
      $_GET[decodeURIComponent(tmp[0])] = decodeURIComponent(tmp.slice(1).join("").replace("+", " "));
    }
  }
  return $_GET;
}


/**
 * Définit un cookie
 * @param {type} c_name
 * @param {type} value
 * @param {type} exdays
 * @returns {undefined}
 */
function setCookie(c_name, value, exdays) {
  //console.log(c_name, value, exdays);
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
  document.cookie = c_name + "=" + c_value;

}

/**
 * Retourne la valeur du cookie
 * @param {type} c_name
 * @returns {Document.cookie|Node.cookie|String|getCookie.c_value}
 */
function getCookie(c_name) {
  var c_value = document.cookie;
  var c_start = c_value.indexOf(" " + c_name + "=");
  if (c_start === -1) {
    c_start = c_value.indexOf(c_name + "=");
  }
  if (c_start === -1) {
    c_value = null;
  } else {
    c_start = c_value.indexOf("=", c_start) + 1;
    var c_end = c_value.indexOf(";", c_start);
    if (c_end === -1) {
      c_end = c_value.length;
    }
    c_value = unescape(c_value.substring(c_start, c_end));
  }
  return c_value;
}
/**
 * Petite classe : coordonnées en x et y
 * @param {type} x
 * @param {type} y
 * @returns {coord}
 */
function Coord(x, y) {
  this.x = x;
  this.y = y;
}

/**
 * Active et désactive le plein ecran de la page web
 * @param {HTMLElement} zonePleinEcran DOM Element devenant fullscreen (DIV, etc)
 * @param {Function} callback Fonction à appeler lors du fullscreen ON
 * @default Body
 *
 * @returns {undefined}
 */

function fullscreenOnOff(zonePleinEcran, callback) {

  if (!isDefined(callback)) { //Bad practice dsl
    if (isDefined(GM.sceneRenderer)) {
      callback = GM.sceneRenderer.draw;
    }
  }

  if ((zonePleinEcran === undefined) || (zonePleinEcran === "")) {
    zonePleinEcran = document.body;
  }

  if (
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement
  ) {
    //on est en plein ecran, donc demande de sortie
    console.log("on est en plein ecran, donc demande de sortie");
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }


  } else {
    //on est pas en plein ecran, donc demande de plein ecran
    console.log("on est pas en plein ecran, donc demande de plein ecran");
    if (
      document.fullscreenEnabled ||
      document.webkitFullscreenEnabled ||
      document.mozFullScreenEnabled ||
      document.msFullscreenEnabled
    ) {
      //plein ecran possible
      console.log("plein ecran possible");

      if (zonePleinEcran.requestFullscreen) {
        zonePleinEcran.requestFullscreen();
      } else if (zonePleinEcran.webkitRequestFullscreen) {
        zonePleinEcran.webkitRequestFullscreen();
      } else if (zonePleinEcran.mozRequestFullScreen) {
        zonePleinEcran.mozRequestFullScreen();
      } else if (zonePleinEcran.msRequestFullscreen) {
        zonePleinEcran.msRequestFullscreen();
      } else {
        console.log("Pb avec zonePleinEcran.requestFullscreen");
        zonePleinEcran.requestFullscreen();
      }

      callback();

    } else {
      console.log("Problème de plein ecran : document.fullscreenEnabled == " + document.fullscreenEnabled);

    }
  }
}

/**
 * Affiche avec un foncdu un element du dom
 * @param {DOMElement} element
 * @returns {undefined}
 */
function showWithNiceFading(element) {
  //    console.log("showWithniceFading", element);
  $(element)

    .css({
      opacity: 0,
      display: "block",
      visibility: "visible"
    })
    .animate({
      opacity: 1
    }, 500, function() {

    });
}

/**
 * Affiche avec un foncdu un element du dom
 * @param {DOMElement} element
 * @returns {undefined}
 */
function hideWithNiceFading(element) {
  //    console.log("showWithniceFading", element);
  $(element)

    .css({
      opacity: 1,
      display: "none",
      visibility: "visible"
    })
    .animate({
      opacity: 0
    }, 500, function() {

    });
}

/**
 * Teste si une valeur est null, undefined, ou chaine vide.
 * Renvoi de true si variable est definie
 * @param {type} variable La variable à tester
 * @returns {Boolean}
 */
function isDefined(variable) {
  return (
    (variable !== null) &&
    (variable !== undefined) &&
    (variable !== "")
  );

}

/**
 * Genere une UID de 16 caracteres
 * @returns {String}
 */
function generateUid() {

  var letters = 'abcdefghijklmnopqrstuvwxyz';
  var numbers = '1234567890';
  var charset = letters + letters.toUpperCase() + numbers;
  var uidLength = 16;

  var R = '';
  for (var i = 0; i < uidLength; i++)
    R += charset[Math.floor(Math.random() * charset.length)];
  return R;

}

/**
 * Détecte si nous sommes en ésence d'un écran tactile
 * @returns {Boolean}
 */
function isTouchDevice() {
  return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

/**
 * Mélange les elements DOM enfant
 * @param {String} identifiants dom, exemple ".divLogo"
 */
function shuffleLogos() {
  var a = document.querySelector(".divLogo");
  //    var b = a.childNodes;
  var b = a.querySelectorAll("a");
  //    console.log("avant", b);

  for (var i = 0; i < b.length; i++) {
    a.removeChild(b[i]);
  }

  var c = new Array();
  for (var j = 0; j < b.length; j++) {
    c.push(b[j]);
  }

  c = shuffle(c);
  for (var ii = 0; ii < b.length; ii++) {
    a.appendChild(c[ii]);
  }
  //    console.log("après", c);

  $(".divLogo")
    //            .css({opacity: 0, visibility: "visible"})
    .css({
      opacity: 0,
      display: "flex",
      visibility: "visible"
    })
    .animate({
      opacity: 1
    }, 500, function() {

    });


}

/**
 * Detection de navigateur
 * @returns {detectBrowser.out} Object
 */
function detectBrowser() {
  var out = {};

  // Opera 8.0+
  out.isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

  // Firefox 1.0+
  out.isFirefox = typeof InstallTrigger !== 'undefined';

  // Safari 3.0+ "[object HTMLElementConstructor]"
  out.isSafari =
    ((navigator.userAgent.indexOf("AppleWebKit") !== -1)
      //            &&(navigator.userAgent.indexOf("Macintosh") !== -1)
      &&
      (navigator.userAgent.indexOf("Safari") !== -1));

  //    out.isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
  //        return p.toString() === "[object SafariRemoteNotification]";
  //    })(!window['safari'] || safari.pushNotification);

  // Internet Explorer 6-11
  out.isIE = /*@cc_on!@*/ false || !!document.documentMode;

  // Edge 20+
  out.isEdge = !out.isIE && !!window.StyleMedia;

  // Chrome 1+
  out.isChrome = !!window.chrome && !!window.chrome.webstore;

  // Blink engine detection
  out.isBlink = (out.isChrome || out.isOpera) && !!window.CSS;

  //Electron
  out.isElectron = navigator.userAgent.indexOf("Electron") !== -1;

  return out;
}

/**
 * Retourne pour affichage le numéro de moi oou du jour correcte, cad avec le 0 SI < 10
 * @param {Number::Integer} n La date en caractère
 * @return {String}
 */
function formatNumberForDateDisplay(n) {
  return ((n < 10) ? "0" : "") + n;
}
/**
 * Retourne pour affichage le numéro de moi oou du jour correcte, cad avec le 0 SI < 10
 * @param {String} n La date en caractère
 * @return {String}
 */
function formatStringNumberForDateDisplay(c) {
  return ((c.length === 1) ? "0" : "") + c;
}

/**
 * Assure affichage "à la française" de la date jj/mm/aaaa
 * @param {Date} date
 * @returns {String}
 */
function formatDateForDisplay(date) {
  if (!(date instanceof Date)) {
    console.warn("Attention probleme de date ", date);
    date = new Date();
  }
  return formatNumberForDateDisplay(date.getDate()) + '/' +
    formatNumberForDateDisplay(date.getMonth() + 1) + '/' +
    date.getFullYear();

}
/**
 * Assure affichage "à la française" de la date mm/aaaa
 * @param {Date} date
 * @returns {String}
 */
function formatDateForSimpleDisplay(date) {
  if (!(date instanceof Date)) {
    console.warn("Attention probleme de date ", date);
    date = new Date();
  }
  //anciennement : content_ok = ((now.toLocaleDateString().split('/')[0].length === 1) ? "0" : "") + now.toLocaleDateString().split('/')[0] + "/" + now.toLocaleDateString().split('/')[2];
  return formatNumberForDateDisplay(date.getMonth() + 1) + '/' +
    date.getFullYear();

}

function rgb2hex(rgb) {
  console.log(rgb)
  if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}