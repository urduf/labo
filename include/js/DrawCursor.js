class DrawCursor {

  cursorShape = "circle";
  cursorSize = 5;
  target;
  fill = "#00000000";
  lineWidth = 2;
  strokeColor = "#000000"

  margin = 5;
  crosshairSize = 10;
  shadowColor = "white";
  shadowBlur = 2;

  setTarget(target) {
    this.target = target;
  }

  init() {
    this.target.canvas.style.cursor = "none";
    // this.target.canvas.style.backgroundColor = "#aabbcc55";
    this.target.canvas.style.position = "absolute";
    this.target.canvas.style.top = "52px";
    this.target.canvas.style.left = "0px";
    // this.target.canvas.style.left: "0px";
    //this.target.canvas.style.top = "-" + this.target.canvas.height + "px";
    this.target.shadowColor = this.shadowColor;
    this.target.shadowBlur = this.shadowBlur;
    // this.target.shadowOffsetX = 0;
    // this.target.shadowOffsetY = 0;
    this.target.lineWidth = this.lineWidth;
    this.target.strokeStyle = this.strokeColor;

  }
  setSize(size) {
    this.cursorSize = parseInt(size, 10);
  }

  draw(x, y) {
    //  console.log(x, y)

    this.target.clearRect(
      0 - this.target.canvas.width / 2,
      0 - this.target.canvas.height / 2,
      this.target.canvas.width,
      this.target.canvas.height);


    this.target.beginPath();
    this.target.arc(x, y, this.cursorSize / 2, 0, 2 * Math.PI, false);
    if (this.cursorSize < 10) {

      this.target.moveTo(
        x,
        y - this.cursorSize - this.margin);
      this.target.lineTo(
        x,
        y - this.cursorSize - this.crosshairSize - this.margin);

      this.target.moveTo(
        x,
        y + this.cursorSize + this.margin);
      this.target.lineTo(
        x,
        y + this.cursorSize + this.crosshairSize + this.margin);

      this.target.moveTo(
        x - this.cursorSize - this.margin,
        y);
      this.target.lineTo(
        x - this.cursorSize - this.crosshairSize - this.margin,
        y);

      this.target.moveTo(
        x + this.cursorSize + this.margin,
        y);
      this.target.lineTo(
        x + this.cursorSize + this.crosshairSize + this.margin,
        y);

    }



    this.target.stroke();
  }
}