/**GESTION HISTORIQUE**/
let currentIndex = 0;

var arrHistory = new Array();

function recordState() {
  // console.log("RecordState", currentIndex, arrHistory.length)


  if (currentIndex == arrHistory.length) {
    currentIndex = arrHistory.length + 1;
  } else {
    //s'il y a eu des retours, on efface histirique posterieur
    console.log("€rasing from ", currentIndex + 1, " for ", arrHistory.length - (currentIndex));
    arrHistory.splice(currentIndex + 1, arrHistory.length - (currentIndex));
    currentIndex = arrHistory.length;
  }
  arrHistory.push(context.getImageData(0, 0, context.canvas.width, context.canvas.height));
}

function undo() {

  if (currentIndex <= 0) {
    //currentIndex = arrHistory.length - 1;
    currentIndex = 0;
  } else {
    if (currentIndex == arrHistory.length) {
      currentIndex -= 2;
    } else {
      currentIndex--;
    }
  }
  console.log("undo", currentIndex)
  restore(currentIndex);
}

function redo() {
  if (currentIndex == 0) {
    // return;
  }
  if (currentIndex == arrHistory.length) {

  }
  if ((currentIndex >= 0) && (currentIndex < arrHistory.length - 1)) {
    currentIndex++;
  }
  console.log("redo", currentIndex)
  restore(currentIndex);
}

function restore(index) {
  let imageData = arrHistory[index];
  context.clearRect(0 - centerX, 0 - centerY, context.canvas.width, context.canvas.height);
  context.putImageData(imageData, 0, 0, 0, 0, context.canvas.width, context.canvas.height);
}

function erase() {
  arrHistory = new Array();
  recordState();
  context.clearRect(0, 0, canvas.width, canvas.height);
  //window.cancelAnimationFrame()
  // recordState();
  init();
}

/**GESTION COLORPICKER**/


let arrColors = [];
let arrColorsShadow = [];

function restorePaletteFromCookie() {

  if (arrColors.length == 0) {


    let arrColorsCookie = getCookie("arrColors");
    if (arrColorsCookie != null) {
      arrColors = arrColorsCookie.split(",");
      for (var i = 0; i < arrColors.length; i++) {

        let newDivColor = document.createElement("div");
        newDivColor.classList.add("is-1");
        newDivColor.style.backgroundColor = arrColors[i];
        newDivColor.onclick = function() {
          let hexColor =
            rgb2hex(newDivColor.style.backgroundColor);
          console.log(
            "setColor",
            newDivColor.style.backgroundColor,
            hexColor);
          document.getElementById("inDrawColor").value = hexColor;
          conf.DRAW_COLOR = hexColor;
        };
        document.querySelector("#divColors").appendChild(newDivColor);
      }
    }
  }

  if (arrColorsShadow.length == 0) {
    console.log(arrColorsShadow)
    let arrColorsShadowCookie = getCookie("arrColorsShadow");
    if (arrColorsShadowCookie != null) {
      arrColorsShadow = arrColorsShadowCookie.split(",");
      for (var i = 0; i < arrColorsShadow.length; i++) {

        let newDivColor = document.createElement("div");
        newDivColor.classList.add("is-1");
        newDivColor.style.backgroundColor = arrColorsShadow[i];
        newDivColor.onclick = function() {
          let hexColor =
            rgb2hex(newDivColor.style.backgroundColor);
          console.log(
            "setColor",
            newDivColor.style.backgroundColor,
            hexColor);
          document.getElementById("inShadowColor").value = hexColor;
          conf.SHADOW_COLOR = hexColor;
        };
        document.querySelector("#divShadowColors").appendChild(newDivColor);
      }
    }
  }
}

function updateColorTable() {
  if (arrColors[arrColors.length - 1] != conf.DRAW_COLOR) {
    let isInHistory = false;
    for (var i = 0; i < arrColors.length; i++) {
      if (arrColors[i] == conf.DRAW_COLOR) {
        isInHistory = true;
      }
    }

    if (!isInHistory) {

      arrColors.push(conf.DRAW_COLOR);
      let newDivColor = document.createElement("div");
      newDivColor.style.backgroundColor = conf.DRAW_COLOR;
      newDivColor.onclick = function() {
        let hexColor =
          rgb2hex(newDivColor.style.backgroundColor);
        console.log(
          "setColor",
          newDivColor.style.backgroundColor,
          hexColor);
        document.getElementById("inDrawColor").value = hexColor;
        conf.DRAW_COLOR = hexColor;
      };
      document.querySelector("#divColors").appendChild(newDivColor);
      setCookie("arrColors", arrColors.toString());

    }
  }
}

function updateColorShadowTable() {
  if (arrColorsShadow[arrColorsShadow.length - 1] != conf.SHADOW_COLOR) {
    let isInHistory = false;
    for (var i = 0; i < arrColorsShadow.length; i++) {
      if (arrColorsShadow[i] == conf.SHADOW_COLOR) {
        isInHistory = true;
      }
    }

    if (!isInHistory) {

      arrColorsShadow.push(conf.SHADOW_COLOR);
      let newDivColor = document.createElement("div");
      newDivColor.style.backgroundColor = conf.SHADOW_COLOR;
      newDivColor.onclick = function() {
        let hexColor =
          rgb2hex(newDivColor.style.backgroundColor);
        console.log(
          "setColor",
          newDivColor.style.backgroundColor,
          hexColor);
        document.getElementById("inShadowColor").value = hexColor;
        conf.SHADOW_COLOR = hexColor;
      };
      document.querySelector("#divShadowColors").appendChild(newDivColor);
      setCookie("arrColorsShadow", arrColors.toString());

    }
  }
}

function resetDrawPosition() {
  context.setTransform(1, 0, 0, 1, 0, 0);
  context.translate(centerX, centerY);
}

function fileUpload(elt) {

  resetDrawPosition();

  console.log(elt)
  let file = elt.files[0];
  console.log(file)
  var img = document.createElement("img");
  var reader = new FileReader();
  reader.onload = function(e) {
    img.src = e.target.result
  }
  reader.readAsDataURL(file);
  console.log(img)
  img.onload = function() {
    context.drawImage(img, 0 - img.naturalWidth / 2, 0 - img.naturalHeight / 2);
    recordState();
  }
}

//Pixel manip canvas et imageData : GET
function getPixel(imgData, index) {
  var i = index * 4,
    d = imgData.data;
  return [d[i], d[i + 1], d[i + 2], d[i + 3]] // returns array [R,G,B,A]
}

// AND/OR

function getPixelXY(imgData, x, y) {
  return getPixel(imgData, y * imgData.width + x);
}