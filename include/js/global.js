function toggleShowFormConfig() {
  console.log("hoho");
  if (document.querySelector("#divFormConfig").classList.contains("is-hidden")) {
    document.querySelector("#divFormConfig").classList.toggle("is-hidden");
    document.querySelector("#btnMenuOn i").classList.replace("fa-bars", "fa-hamburger");
  } else {
    document.querySelector("#divFormConfig").classList.toggle("is-hidden");
    document.querySelector("#btnMenuOn i").classList.replace("fa-hamburger", "fa-bars");
  }
}

function toggleFullSizeCanvas() {

  if (document.getElementById("inCanvasWidth") != null) {
    document.getElementById("inCanvasWidth").value = document.body.clientWidth;
    document.getElementById("inCanvasHeight").value = document.documentElement.clientHeight;
    updateConfig();
  } else {
    conf.CANVAS_WIDTH = document.body.clientWidth;
    conf.CANVAS_HEIGHT = document.documentElement.clientHeight;
    init();
  }

}

function toggleFullscreen() {


  if (!document.fullscreenElement) {
    document.documentElement.requestFullScreen();
  } else {

    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }

}

function saveImage() {
  var image = canvas.toDataURL("image/png")
    .replace("image/png", "image/octet-stream");
  // here is the most important part because if you dont replace you will get a DOM 18 exception.

  window.location.href = image;
}

function randomColor() {
  var hexColor = "#";
  var rCol = Math.floor(Math.random() * 256);
  var vCol = Math.floor(Math.random() * 256);
  var bCol = Math.floor(Math.random() * 256);
  hexColor += toHex256(rCol) + toHex256(vCol) + toHex256(bCol);
  return hexColor;
}


function toHex256(intVal) {
  if (intVal <= 0) {
    return "00";
  }
  var hex = intVal.toString(16).toUpperCase();
  if (hex.length < 2) {
    hex = "0" + hex;
  }
  return hex;
}



/*********************/
document.addEventListener('DOMContentLoaded', () => {

  document.querySelector(".burger").addEventListener('click', () => {

    // Get the target from the "data-target" attribute
    const target = document.querySelector(".burger").dataset.target;
    const $target = document.getElementById(target);

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    document.querySelector(".burger").classList.toggle('is-active');
    $target.classList.toggle('is-active');

  });
});