var conf = {
  hasStat: false,
  CANVAS_WIDTH: 830,
  CANVAS_HEIGHT: 585,
  MAX_ITERATIONS: 255,
  //milliseceonds
  TICK_TIME: 80,
  SIZE_STEP: .7,
  FILL_STYLE: '#ffbbbb',
  //0..256
  LINE_WIDTH: 0.7,

  DRAW_COLOR: "#000000",
  DRAW_ALPHA: "ff",
  DRAW_SIZE: 1,
  SPEED: 50,
  MAX_FREQ: 4000,
  MIN_FREQ: 400
};

var context, centerX, centerY, canvas, cursorCanvas, drawCursor, progressCanvas;

let audioContext,
  oscillator,
  gainNode;

let imageData;

var maxFreq = 6000;
var maxVol = 0.02;

var initialFreq = 3000;
var initialVol = 0.00;

var progress = 0;

var objArray = [];

var onPause = false;

var isStarted = false;

var arrPoints = new Array();
var arrLines = new Array();

var penIsDown = false;
var stat;

function init() {
  console.log("INIT");



  drawCursor = new DrawCursor();

  canvas = document.getElementById("drawCanvas");
  context = canvas.getContext('2d');

  cursorCanvas = document.getElementById("cursorCanvas");
  contextCursorCanvas = cursorCanvas.getContext('2d');

  progressCanvas = document.getElementById("progressCanvas");
  contextProgressCanvas = progressCanvas.getContext('2d');

  drawCursor.setTarget(contextCursorCanvas);
  drawCursor.setSize(conf.DRAW_SIZE);

  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  cursorCanvas.width = conf.CANVAS_WIDTH;
  cursorCanvas.height = conf.CANVAS_HEIGHT;

  progressCanvas.width = conf.CANVAS_WIDTH;
  progressCanvas.height = conf.CANVAS_HEIGHT;

  drawCursor.init();

  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;

  context.translate(centerX, centerY);
  contextCursorCanvas.translate(centerX, centerY);

  context.lineCap = "round";
  context.lineJoin = 'round';

  cursorCanvas.onmousedown = function(evt) {
    mousedown(evt);
  };
  cursorCanvas.onmouseup = function(evt) {
    mouseup(evt);
  };
  cursorCanvas.onmousemove = function(evt) {
    mousemove(evt);
  };
  cursorCanvas.onmouseout = function(evt) {
    mouseout(evt);
  };

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }

  document.querySelectorAll("#divFormConfig input.updater").forEach(function(elem) {
    elem.onchange = updateConfig;
  });

  document.querySelector("#inDrawSizeRange").onchange = function(elem) {
    document.querySelector("#inDrawSize").value = document.querySelector("#inDrawSizeRange").value;
    updateConfig();
  };
  document.querySelector("#btnPlay").onclick = function(elem) {
    play();
  }
  document.querySelector("#btnPause").onclick = function(elem) {
    pause();
  }

  initAudio();

}


function mousedown(evt) {
  // console.log(evt);
  arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
  penIsDown = true;
}

function mousemove(evt) {
  //console.log(evt);
  if (penIsDown) {
    arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
    drawLine();
  }
  drawCursor.draw(evt.layerX - centerX, evt.layerY - centerY);
}


function mouseup(evt) {
  //console.log(evt);
  penIsDown = false;
  //console.log("Sending", arrPoints.length, "points", arrPoints);
  arrLines.push(arrPoints);
  recordState();
  arrPoints = new Array();
}

function mouseout(evt) {
  if (penIsDown) {
    mouseup(evt);
  }
}


function drawLine() {

  context.shadowColor = conf.SHADOW_COLOR;
  context.shadowBlur = conf.SHADOW_SIZE;

  if (arrPoints.length > 1) {
    context.beginPath();
    context.strokeStyle = conf.DRAW_COLOR + conf.DRAW_ALPHA;
    context.lineWidth = conf.DRAW_SIZE;
    context.moveTo(arrPoints[arrPoints.length - 2][0], arrPoints[arrPoints.length - 2][1]);
    context.lineTo(arrPoints[arrPoints.length - 1][0], arrPoints[arrPoints.length - 1][1]);
    context.stroke();

  }


}


function updateConfig() {
  console.log("updateConfig")
  conf.DRAW_SIZE = document.getElementById("inDrawSize").value;
  conf.DRAW_COLOR = document.getElementById("inDrawColor").value;
  conf.DRAW_ALPHA = toHex256(parseInt(document.getElementById("inDrawAlpha").value));
  conf.SHADOW_SIZE = parseInt(document.getElementById("inShadowSize").value);
  conf.SHADOW_COLOR = document.getElementById("inShadowColor").value;

  conf.ROTATION_GAP = parseInt(document.getElementById("inRotationGap").value);
  drawCursor.setSize(document.getElementById("inDrawSize").value);
  resetDrawPosition();
}


function start() {
  isStarted = true;
  tick();
}

function tick() {
  console.log(progress);
  if (onPause) return;
  if (progress >= conf.CANVAS_WIDTH) {
    isStarted = false;
    onPause = true;
    progress = 0;
    pause();
    return;
  }

  drawProgress();

  playSoundIfNeeded();

  progress++;

  window.requestAnimationFrame(tick);


  //console.log(progress++, objArray.length);
}


function play() {
  //  oscillator.start(0);
  imageData = context.getImageData(0, 0, conf.CANVAS_WIDTH, conf.CANVAS_HEIGHT);

  onPause = false;
  isStarted = true;

  tick();

}

function pause() {
  //oscillator.stop();
  gainNode.gain.value = 0;
  onPause = true;
}

function drawProgress() {
  console.log(progress);

  // /!\ attention ce canvas n'a pas eu de translate, pas besoin de calculs savants
  contextProgressCanvas.clearRect(
    0,
    0,
    conf.CANVAS_WIDTH,
    conf.CANVAS_HEIGHT);


  contextProgressCanvas.strokeStyle = conf.DRAW_COLOR + conf.DRAW_ALPHA;
  contextProgressCanvas.lineWidth = conf.DRAW_SIZE;
  contextProgressCanvas.beginPath();
  contextProgressCanvas.moveTo(0 + progress,
    0);

  contextProgressCanvas.lineTo(0 + progress,
    conf.CANVAS_HEIGHT);
  contextProgressCanvas.stroke();

}

function initAudio() {
  // create web audio api context
  audioContext = new AudioContext();

  // create Oscillator and gain node
  oscillator = audioContext.createOscillator();
  gainNode = audioContext.createGain();

  // connect oscillator to gain node to speakers

  oscillator.connect(gainNode);
  gainNode.connect(audioContext.destination);


  oscillator.detune.value = 100; // value in cents
  oscillator.start(0);

  oscillator.onended = function() {
    console.log('Your tone has now stopped playing!');
  };

  gainNode.gain.value = initialVol;
  gainNode.gain.minValue = initialVol;
  gainNode.gain.maxValue = initialVol;


}

function playSoundIfNeeded() {
  //console.log("playSoundIfNeeded");
  //pas optimisé : 1 image data pour chq colonne... a améliorer
  let hasSound = false;

  for (var i = 0; i < imageData.height; i++) {
    let pixel = getPixelXY(imageData, progress, i); //[R,G,B,A]
    //  console.log(i, pixel)

    if (pixel[0] > 0) {
      console.log(0, pixel, pixel[0], progress, i);
    }
    if (pixel[1] > 0) {
      console.log(1, pixel, pixel[1], progress, i);
    }
    if (pixel[2] > 0) {
      console.log(2, pixel, pixel[2], progress, i);
    }
    if (pixel[3] > 0) {
      console.log(3, pixel, pixel[3], progress, i);
      playSound(
        (conf.CANVAS_HEIGHT - i) / conf.CANVAS_HEIGHT,
        pixel[3] / 254);
      hasSound = true;

    }

  }
  if (!hasSound) {
    gainNode.gain.value = 0;
  }

}

function playSoundFromPixel() {

}

function playSound(freq, gain) {
  console.log("playSound", freq, gain);
  gainNode.gain.value = gain * maxVol;
  oscillator.frequency.value = freq * maxFreq;
}

function uploadPartition() {}