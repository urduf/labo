var conf = {
  hasStat: false,
  CANVAS_WIDTH: 830,
  CANVAS_HEIGHT: 585,
  MAX_ITERATIONS: 255,
  //milliseceonds
  TICK_TIME: 80,
  RADIUS: 110,
  DIVERGENCE_RATIO: 12.3,
  SIZE_STEP: .7,
  MAX_BRANCHES: 780,
  FILL_STYLE: '#ffbbbb',
  //0..256
  FILL_ALPHA: 164,
  RANDOM_COLOR: true,
  STROKE_COLOR: '#333333',
  HAS_STROKE: false,
  LINE_WIDTH: 0.7,
  DIVISION_CHANCES: 04,
  OVERFLOW: false,
  BORDER_SIZE: 10,
  FIXED_DIVERGENCE: true,
  ROTATION_GAP: 2,
  START_X: 10,
  START_Y: 0
};

var context, centerX, centerY, canvas;


var iteratorCount = 0;

var objArray = [];

var onPause = false;

var isStarted = false;

var arrPoints = new Array();
var arrLines = new Array();

var penIsDown = false;
var stat;

function init() {
  console.log("INIT");

  canvas = document.getElementById("treeCanvas");
  context = canvas.getContext('2d');

  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;

  canvas.onmousedown = function(evt) {
    mousedown(evt);
  };
  canvas.onmouseup = function(evt) {
    mouseup(evt);
  };
  canvas.onmousemove = function(evt) {
    mousemove(evt);
  };



  var obj = {
    x: conf.START_X,
    y: conf.START_Y,
    radius: conf.RADIUS,
    color: conf.FILL_STYLE,
    rotation: 0
  };

  console.log(obj);

  objArray.push(obj);

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }
  //context.translate(centerX, centerY);

  if (!isStarted) {

    start();
  }
}

function start() {
  isStarted = true;
  tick();
}

function tick() {
  if (onPause) return;
  if (iteratorCount == conf.MAX_ITERATIONS) return;



  window.requestAnimationFrame(tick);
  if (conf.hasStat) {
    stat.update();
  }

  //console.log(iteratorCount++, objArray.length);
}


function drawCircleObj(obj) {
  drawCircle(obj.x, obj.y, obj.radius, obj.color);
}

function mousedown(evt) {
  console.log(evt);
  penIsDown = true;
}

function mousemove(evt) {
  //console.log(evt);
  if (penIsDown) {
    arrPoints.push([evt.layerX, evt.layerY]);
    if (arrPoints.length > 1) {
      context.beginPath();
      context.moveTo(arrPoints[arrPoints.length - 2][0], arrPoints[arrPoints.length - 2][1]);
      context.lineTo(arrPoints[arrPoints.length - 1][0], arrPoints[arrPoints.length - 1][1]);
      context.stroke();
    }
  }
}

function mouseup(evt) {
  //console.log(evt);
  penIsDown = false;
  //console.log("Sending", arrPoints.length, "points", arrPoints);
  arrLines.push(arrPoints);
  arrPoints = new Array();
}


function redraw() {
  objArray = [];
  context.clearRect(0, 0, canvas.width, canvas.height);
  //window.cancelAnimationFrame()
  init();
}


function updateConfig() {

}