var conf = {
  hasStat: false,
  CANVAS_WIDTH: 830,
  CANVAS_HEIGHT: 585,
  MAX_ITERATIONS: 255,
  //milliseceonds
  TICK_TIME: 80,
  RADIUS: 110,
  SIZE_STEP: .7,
  MAX_BRANCHES: 780,
  FILL_STYLE: '#ffbbbb',
  //0..256
  LINE_WIDTH: 0.7,
  ROTATION_GAP: 40,
  DRAW_COLOR: "#000000",
  DRAW_ALPHA: 164,
  DRAW_SIZE: 1,
  SHADOW_COLOR: "#000000",
  SHADOW_SIZE: 30
};

var context, centerX, centerY, canvas, cursorCanvas, drawCursor;


var iteratorCount = 0;

var objArray = [];

var onPause = false;

var isStarted = false;

var arrPoints = new Array();
var arrLines = new Array();

var penIsDown = false;
var stat;


function init() {
  console.log("INIT");


  restorePaletteFromCookie();

  drawCursor = new DrawCursor();

  arrColors.push(conf.DRAW_COLOR);

  canvas = document.getElementById("drawCanvas");
  context = canvas.getContext('2d');

  context.shadowColor = conf.SHADOW_COLOR;
  context.shadowBlur = conf.SHADOW_SIZE;



  cursorCanvas = document.getElementById("cursorCanvas");
  contextCursorCanvas = cursorCanvas.getContext('2d');

  drawCursor.setTarget(contextCursorCanvas);
  drawCursor.setSize(conf.DRAW_SIZE);

  canvas.width = conf.CANVAS_WIDTH;
  canvas.height = conf.CANVAS_HEIGHT;

  cursorCanvas.width = conf.CANVAS_WIDTH;
  cursorCanvas.height = conf.CANVAS_HEIGHT;

  drawCursor.init();

  centerX = canvas.clientWidth / 2;
  centerY = canvas.clientHeight / 2;

  context.translate(centerX, centerY);
  contextCursorCanvas.translate(centerX, centerY);

  context.lineCap = "round";
  context.lineJoin = 'round';

  recordState();

  cursorCanvas.onmousedown = function(evt) {
    mousedown(evt);
  };
  cursorCanvas.onmouseup = function(evt) {
    mouseup(evt);
  };
  cursorCanvas.onmousemove = function(evt) {
    mousemove(evt);
  };
  cursorCanvas.onmouseout = function(evt) {
    mouseout(evt);
  };

  if (conf.hasStat) {

    stat = new Stats();
    stat.domElement.setAttribute("class", 'statGui');
    if (document.querySelectorAll(".statGui").length == 0)
      document.body.appendChild(stat.domElement);
  }

  document.querySelectorAll("#divFormConfig input.updater").forEach(function(elem) {
    elem.onchange = updateConfig;
  });

  document.querySelector("#inDrawSizeRange").onchange = function(elem) {
    document.querySelector("#inDrawSize").value = document.querySelector("#inDrawSizeRange").value;
    updateConfig();
  };

}


function mousedown(evt) {
  // console.log(evt);
  arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
  penIsDown = true;
}

function mousemove(evt) {
  //console.log(evt);
  if (penIsDown) {
    arrPoints.push([evt.layerX - centerX, evt.layerY - centerY]);
    drawLine();
  }
  drawCursor.draw(evt.layerX - centerX, evt.layerY - centerY);
}


function mouseup(evt) {
  //console.log(evt);
  penIsDown = false;
  //console.log("Sending", arrPoints.length, "points", arrPoints);
  arrLines.push(arrPoints);
  recordState();
  arrPoints = new Array();
}

function mouseout(evt) {
  if (penIsDown) {
    mouseup(evt);
  }
}


function drawLine() {


  context.shadowColor = conf.SHADOW_COLOR;
  context.shadowBlur = conf.SHADOW_SIZE;
  //  context.save();
  let nbIterations = Math.ceil(360 / conf.ROTATION_GAP)

  // console.log("nbIter", nbIterations);
  // console.log(arrPoints);
  for (var i = 0; i < nbIterations; i++) {
    // console.log(i, arrPoints.length);
    if (arrPoints.length > 1) {
      context.beginPath();
      context.strokeStyle = conf.DRAW_COLOR + conf.DRAW_ALPHA;
      context.lineWidth = conf.DRAW_SIZE;
      context.moveTo(arrPoints[arrPoints.length - 2][0], arrPoints[arrPoints.length - 2][1]);
      context.lineTo(arrPoints[arrPoints.length - 1][0], arrPoints[arrPoints.length - 1][1]);
      context.stroke();

      context.rotate(conf.ROTATION_GAP * Math.PI / 180);

    }
  }
  //  context.restore();

}


function updateConfig() {
  console.log("updateConfig")
  conf.DRAW_SIZE = document.getElementById("inDrawSize").value;
  conf.DRAW_COLOR = document.getElementById("inDrawColor").value;
  conf.DRAW_ALPHA = toHex256(parseInt(document.getElementById("inDrawAlpha").value));
  conf.SHADOW_SIZE = parseInt(document.getElementById("inShadowSize").value);
  conf.SHADOW_COLOR = document.getElementById("inShadowColor").value;

  conf.ROTATION_GAP = parseInt(document.getElementById("inRotationGap").value);
  updateColorTable();
  updateColorShadowTable();
  drawCursor.setSize(document.getElementById("inDrawSize").value);
  resetDrawPosition();
}